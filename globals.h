
#ifndef VSFTP_GLOBALS_H
#define VSFTP_GLOBALS_H

#include <stdlib.h>
#include <pthread.h>

struct sNamedPipe 
{
  int active;
  int fd;
  pthread_mutex_t lock;
};

extern struct sNamedPipe namedPipe;

#endif
